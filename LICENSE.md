# LICENSE

This theme is openly licensed via [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/)

## Original LICENSE

License
=======

ALLOWED:

					  - Share and duplicate as it is

					  - Edit, alter, change it

REQUIREMENTS:

					  - Attribution, give credit to the creator

					  - Indicate changes to it

					  - Publish the changes under the same license

PROHIBITED:

					  - Commercial distribution

LOGO NOTICE:
The used logos and trademarks are copyright of their respective owners.